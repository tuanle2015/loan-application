#  Loan Application Service Homework #

### The project contains: ###

* All APIs related to loan application: list loan, find loan by id, create loan, delete loan, update loan's mark

# Get Started #

### Prerequisites ###

You must have: 

* JDK 11
* Maven

### Build The Library ###

* build

	### Window ####
		mvnw clean package

	#### Linux ####
		./mvnw clean package
		

## Running ##

### Window ####
			mvnw spring-boot:run
			or java -jar target\homework-0.0.1-SNAPSHOT.jar
			
#### Linux ####
			./mvnw spring-boot:run
			or or java -jar target/homework-0.0.1-SNAPSHOT.jar
	
## Test APIs tool ##
* Postman
    





