package com.test.twino.homework.service;

import com.test.twino.homework.dto.LoanDto;
import com.test.twino.homework.entity.Loan;
import com.test.twino.homework.exception.ResourceNotFoundException;

import java.util.List;
import java.util.Optional;


public interface LoanService {

    Optional<Loan> findById(int loanId);

    List<Loan> findAll(String sortBy, String orderBy);

    Loan create(LoanDto loanDto);

    Loan updateMarkById(int loanId, String mark) throws ResourceNotFoundException;

    void deleteById(int loadId) throws ResourceNotFoundException;
}
