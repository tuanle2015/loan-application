package com.test.twino.homework.controller;

import com.test.twino.homework.response.RestEnvelope;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeController {
    @GetMapping("/")
    public ResponseEntity<RestEnvelope> welcome(){
        return new ResponseEntity<>(new RestEnvelope("Welcome to Loan application service"), HttpStatus.OK);
    }
}
