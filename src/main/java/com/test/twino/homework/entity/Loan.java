package com.test.twino.homework.entity;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Date;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "loan")
public class Loan {
    @Id @Setter(AccessLevel.PROTECTED)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String personalID;
    private String firstName;
    private String lastName;
    private LocalDate birthDate;
    private String employer;
    private double salary;
    private double monthlyLiability;
    private double requestedAmount;
    private int requestedTerm;
    private double creditScore;
    private String mark;
    private int createdBy;
    private Date createdDate;
}


