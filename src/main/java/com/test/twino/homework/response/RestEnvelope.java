package com.test.twino.homework.response;

public class RestEnvelope<T> {
    private T data;
    public RestEnvelope(T data) {
        this.data = data;
    }

    public RestEnvelope(T data, T extra) {
        this.data = data;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
